---
layout: handbook-page-toc
title: "Leadership Recurring Check-Ins"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## What is a Leadership Recurring Check-In Call?

The Leadership Recurring Check-In call is a strategic discussion with customer leadership to ensure we are progressing toward our success plan objectives together. While an Executive Business Review (EBR) aims to establish businesss goals and report on progress annually, the Leadership Recurring Check-In allows for a more focused conversation with leadership to allow for progress checks, and review the prioritization of objectives. It is less granular than a cadence call, but more focused than an EBR, and still has the aims of being strategic and requiring company leadership, even if they are not at the executive level.

TAMs should hold optional Leadership Recurring Check-Ins on a quarterly cadence with leadership within development and DevOps teams. These discussions should be anywhere from 30-60 minutes and allow time for the TAM to provide success plan updates and level-set on the customer's goals.

## How to bring up a Leadership Recurring Check-In with your customers

An ideal time to schedule a recurring check-in call is following an EBR, since leadership should be present on the call and have a vested interest in scheduling a check-in call one quarter out. The TAM should bring up the topic of the Leadership Recurring Check-In as an opportunity to track progress toward the established goals during the EBR, and pose it as a conversation on both progress and prioritization of the goals as time passes.
