---
layout: handbook-page-toc
title: Crash Course Stage Resources
---

# On this page
{:.no_toc}

- TOC
{:toc}

# Objectives for this Page

This page is intended to provide a crash course style overview of the most important Product Analytics related resources for each product Stage.
As a Product Analyst or other curious GitLab team member, it can be helpful to have a quick and easy reference for each product Stage to quickly understand high-level functionality, key objectives or a distilled product roadmap, and key data resources currently used under a specific Stage or Group within GitLab before jumping in to an analysis.
If this page serves it's purpose, Product Analysts should be able to visit this handbook page before working with a Stage or Group to obtain helpful contextual information without needing to do a scavenger hunt search across dozens of handbook pages to find relevant information. 

# Product Stages Grouped by Product Section

## Dev Section

### Manage Stage
_Short description of Manage Stage_

<details markdown="1"><summary>Click to expand</summary>


#### Top dashboards referenced by this team

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

#### Important data documentation

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_SMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_GMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_Other_

#### Helpful user research and demos

[Link to user research or demos]()
_Group specification if applicable, brief description of youtube channel linked or specific singular video resource_

#### Product roadmap link

[Link to product roadmap]()
_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page]()
_Resource to understand the current OKRs for this team_

#### Key handbook pages

[Link to helpful handbook page]()
_Describe why this handbook page is helpful for product analysts_

#### Slack Channels

**#slack-channel**
_Description_

#### Team Heirarchy

[Link to team heirarchy]()

</details>

### Plan Stage
_Short description of Plan Stage_

<details markdown="1"><summary>Click to expand</summary>
#### Top dashboards referenced by this team

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

#### Important data documentation

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_SMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_GMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_Other_

#### Helpful user research and demos

[Link to user research or demos]()
_Group specification if applicable, brief description of youtube channel linked or specific singular video resource_

#### Product roadmap link

[Link to product roadmap]()
_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page]()
_Resource to understand the current OKRs for this team_

#### Key handbook pages

[Link to helpful handbook page]()
_Describe why this handbook page is helpful for product analysts_

#### Slack Channels

**#slack-channel**
_Description_

#### Team Heirarchy

[Link to team heirarchy]()

</details>

### Create Stage
_Short description of Create Stage_

<details markdown="1"><summary>Click to expand</summary>

#### Top dashboards referenced by this team

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

#### Important data documentation

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_SMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_GMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_Other_

#### Helpful user research and demos

[Link to user research or demos]()
_Group specification if applicable, brief description of youtube channel linked or specific singular video resource_

#### Product roadmap link

[Link to product roadmap]()
_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page]()
_Resource to understand the current OKRs for this team_

#### Key handbook pages

[Link to helpful handbook page]()
_Describe why this handbook page is helpful for product analysts_

#### Slack Channels

**#slack-channel**
_Description_

#### Team Heirarchy

[Link to team heirarchy]()

</details>

## Ops Section

### Verify Stage
_Short description of Verify Stage_

<details markdown="1"><summary>Click to expand</summary>

#### Top dashboards referenced by this team

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

#### Important data documentation

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_SMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_GMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_Other_

#### Helpful user research and demos

[Link to user research or demos]()
_Group specification if applicable, brief description of youtube channel linked or specific singular video resource_

#### Product roadmap link

[Link to product roadmap]()
_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page]()
_Resource to understand the current OKRs for this team_

#### Key handbook pages

[Link to helpful handbook page]()
_Describe why this handbook page is helpful for product analysts_

#### Slack Channels

**#slack-channel**
_Description_

#### Team Heirarchy

[Link to team heirarchy]()

</details>

### Package Stage
_The Package team works on the package and container registries._

<details markdown="1"><summary>Click to expand</summary>

#### Top dashboards referenced by this team.

[Package GitLab.com Stage Activity Dashboard](https://app.periscopedata.com/app/gitlab/527857/Package-GitLab.com-Stage-Activity-Dashboard)

_Primarily time series analyses for Package features on GitLab.com_

[Package: User Adoption and Growth](https://app.periscopedata.com/app/gitlab/805350/Package:-User-Adoption-and-Growth)

_Primarily time series analyses including both SaaS and SM usage of Package features_

[Package customer adoption](https://app.periscopedata.com/app/gitlab/877343/Package-customer-adoption)

_Customer specific data tables regarding specific Package feature usage_

[Package: Costs](https://app.periscopedata.com/app/gitlab/1011032/Package:-Costs)

_WIP dashboard analyzing GCP costs associated with general 'registry' costs. There are outstanding questions about GCP tables that may impact these analyses in the future._

#### Important data documentation

``` sql

SELECT * 
FROM table
;
```
_SMAU logic_

``` sql

SELECT * 
FROM table
;
```
_GMAU logic_


#### Helpful user research and demos

[User Interviews YouTube Channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KpxCv3B5S-6LFCpBB6NCnga)

_General and feature specific user interviews for the Package team_

[Demos and Speedruns](https://about.gitlab.com/handbook/engineering/development/ops/package/#demos--speedruns)

_Package Handbook section with feature and roadmap demos_

#### Product roadmap link

[Link to product roadmap](https://about.gitlab.com/handbook/engineering/development/ops/package/#roadmap)

_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page](https://about.gitlab.com/handbook/engineering/development/ops/package/#okrs)

_Resource to understand the current OKRs for this team_

#### Key documentation

[Main Package Team Handbook Page](https://about.gitlab.com/handbook/engineering/development/ops/package)

_It can be helpful to search for specific topics on the Package team's main page_

[GitLab Docs Package Page](https://docs.gitlab.com/ee/administration/packages/)

_GitLab Docs are awesome!_

#### Slack Channels

**#s_package** 

#### Team Heirarchy

[List of team members to contact if needed](https://about.gitlab.com/handbook/engineering/development/ops/package/#team-members)

</details>

### Release Stage
_Short description of Release Stage_

<details markdown="1"><summary>Click to expand</summary>

#### Top dashboards referenced by this team

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

#### Important data documentation

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_SMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_GMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_Other_

#### Helpful user research and demos

[Link to user research or demos]()
_Group specification if applicable, brief description of youtube channel linked or specific singular video resource_

#### Product roadmap link

[Link to product roadmap]()
_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page]()
_Resource to understand the current OKRs for this team_

#### Key handbook pages

[Link to helpful handbook page]()
_Describe why this handbook page is helpful for product analysts_

#### Slack Channels

**#slack-channel**
_Description_

#### Team Heirarchy

[Link to team heirarchy]()

</details>

### Configure Stage
_Short description of Configure Stage_

<details markdown="1"><summary>Click to expand</summary>

#### Top dashboards referenced by this team

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

#### Important data documentation

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_SMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_GMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_Other_

#### Helpful user research and demos

[Link to user research or demos]()
_Group specification if applicable, brief description of youtube channel linked or specific singular video resource_

#### Product roadmap link

[Link to product roadmap]()
_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page]()
_Resource to understand the current OKRs for this team_

#### Key handbook pages

[Link to helpful handbook page]()
_Describe why this handbook page is helpful for product analysts_

#### Slack Channels

**#slack-channel**
_Description_

#### Team Heirarchy

[Link to team heirarchy]()

</details>

### Monitor Stage
_Short description of Monitor Stage_

<details markdown="1"><summary>Click to expand</summary>

#### Top dashboards referenced by this team

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

#### Important data documentation

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_SMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_GMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_Other_

#### Helpful user research and demos

[Link to user research or demos]()
_Group specification if applicable, brief description of youtube channel linked or specific singular video resource_

#### Product roadmap link

[Link to product roadmap]()
_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page]()
_Resource to understand the current OKRs for this team_

#### Key handbook pages

[Link to helpful handbook page]()
_Describe why this handbook page is helpful for product analysts_

#### Slack Channels

**#slack-channel**
_Description_

#### Team Heirarchy

[Link to team heirarchy]()

</details>

## Sec Section


### Secure Stage
_Short description of Secure Stage_

<details markdown="1"><summary>Click to expand</summary>

#### Top dashboards referenced by this team

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

#### Important data documentation

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_SMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_GMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_Other_

#### Helpful user research and demos

[Link to user research or demos]()
_Group specification if applicable, brief description of youtube channel linked or specific singular video resource_

#### Product roadmap link

[Link to product roadmap]()
_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page]()
_Resource to understand the current OKRs for this team_

#### Key handbook pages

[Link to helpful handbook page]()
_Describe why this handbook page is helpful for product analysts_

#### Slack Channels

**#slack-channel**
_Description_

#### Team Heirarchy

[Link to team heirarchy]()

</details>


### Protect Stage
_Short description of Protect Stage_

<details markdown="1"><summary>Click to expand</summary>

#### Top dashboards referenced by this team

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

[Link to a dashboard]()
_Group specification if applicable, brief description of dashboard and how it's used_

#### Important data documentation

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_SMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_GMAU_

``` sql

SELECT * 
FROM table
WHERE field = stage_name
;
```
_Other_

#### Helpful user research and demos

[Link to user research or demos]()
_Group specification if applicable, brief description of youtube channel linked or specific singular video resource_

#### Product roadmap link

[Link to product roadmap]()
_Resource to understand the long-term goals for this team_

#### OKRs

[Link to OKRs handbook page]()
_Resource to understand the current OKRs for this team_

#### Key handbook pages

[Link to helpful handbook page]()
_Describe why this handbook page is helpful for product analysts_

#### Slack Channels

**#slack-channel**
_Description_

#### Team Heirarchy

[Link to team heirarchy]()

</details>
